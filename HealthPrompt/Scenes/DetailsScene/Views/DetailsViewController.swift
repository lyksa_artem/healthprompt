//
//  DetailsViewController.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit
import RxSwift

class DetailsViewController: BaseViewController {
    
    var viewModel: DetailsViewModel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupObservables()
        setupViewsOnLoad()
    }
    
    private func setupViewsOnLoad() {
        navigationItem.title = "Event".localized
    }
    
    private func setupObservables() {
        viewModel.timelineEventTitle
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
    }
}
