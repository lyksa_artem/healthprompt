//
//  DetailsCoordinator.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit
import RxSwift

class DetailsCoordinator: Coordinator {
    
    var navigationController: UINavigationController
    
    lazy var childCoordinators: [Coordinator] = []
    lazy var disposeBag = DisposeBag()
    
    var pop: Observable<Void> {
        return popSubject.asObservable()
    }
    
    private lazy var popSubject = PublishSubject<Void>()
    
    private var timelineEvent: TimelineEvent
    
    init(navigationController: UINavigationController, timelineEvent: TimelineEvent) {
        self.navigationController = navigationController
        self.timelineEvent = timelineEvent
    }
    
    func start() {
        let viewController = DetailsViewController.instantiate()
        viewController.viewModel = DetailsViewModel(timelineEvent: timelineEvent)
        
        viewController.pop
            .bind(to: popSubject)
            .disposed(by: disposeBag)
        
        navigationController.pushViewController(viewController, animated: true)
        navigationController.setNavigationBarHidden(false, animated: false)
    }
    
}
