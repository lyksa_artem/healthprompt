//
//  DetailsViewModel.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class DetailsViewModel: BaseViewModel {
    
    var timelineEventTitle: Observable<String> {
        return timelineEvent.map({ $0.title })
    }
    
    private let timelineEvent: BehaviorRelay<TimelineEvent>
    
    init(timelineEvent: TimelineEvent) {
        self.timelineEvent = BehaviorRelay(value: timelineEvent)
        super.init()
    }
    
    
}
