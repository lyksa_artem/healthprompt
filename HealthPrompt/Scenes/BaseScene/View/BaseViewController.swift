//
//  BaseViewController.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BaseViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    
    var pop: Observable<Void> {
        return popSubject.asObservable()
    }
    
    private lazy var popSubject = PublishSubject<Void>()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Keep track of moving back
        if isMovingFromParent {
            popSubject.onNext(())
        }
    }
    
    func setupBaseObservables(_ baseViewModel: BaseViewModel) {
        
        setupSpinner(drivenBy: baseViewModel.isLoadingSubject)
        
        baseViewModel.error
            .flatMap({ Observable.from(optional: $0) })
            .bind(to: rx.errorAlert)
            .disposed(by: disposeBag)
    }
    
}
