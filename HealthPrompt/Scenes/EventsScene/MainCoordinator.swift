//
//  EventsCoordinator.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit
import RxSwift

class MainCoordinator: Coordinator {
    
    private let window: UIWindow?
    var navigationController: UINavigationController
    
    lazy var childCoordinators: [Coordinator] = []
    lazy var disposeBag = DisposeBag()
    lazy var networkService = NetworkService(requestRealizer: RequestRealizer())
    
    var pop: Observable<Void> {
        return popSubject.asObservable()
    }
    
    private lazy var popSubject = PublishSubject<Void>()
    
    init(window: UIWindow?, navigationController: UINavigationController) {
        self.window = window
        self.navigationController = navigationController
    }
    
    func start() {
        guard let window = window else {
            return
        }
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        // Inject dependencies
        
        let viewController = EventsViewController.instantiate()
        let viewModel = EventsViewModel(networkService: networkService)
        
        viewController.viewModel = viewModel
        viewController.setupBaseObservables(viewModel)
        
        viewController.viewModel?.selectedEvent
            .subscribe(onNext: { [weak self] event in
                self?.eventSelected(event)
            }).disposed(by: viewController.disposeBag)
        
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.pushViewController(viewController, animated: true)
    }
}

extension MainCoordinator {
    
    func eventSelected(_ event: TimelineEvent) {
        let detailsCoordinator = DetailsCoordinator(navigationController: navigationController, timelineEvent: event)

        childCoordinators.append(detailsCoordinator)
        detailsCoordinator.start()

        detailsCoordinator.pop.subscribe(onNext: { [weak self] in
            self?.navigationController.setNavigationBarHidden(true, animated: false)
            self?.childCoordinators.removeLast()
        }).disposed(by: detailsCoordinator.disposeBag)
    }
}

