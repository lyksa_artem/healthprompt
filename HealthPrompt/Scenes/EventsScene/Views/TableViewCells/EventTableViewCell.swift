//
//  EventTableViewCell.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Make circle
        leftView.layer.cornerRadius = leftView.frame.size.width / 2
        leftView.layer.masksToBounds = true
    }
    
    func configure(with item: EventItem) {
        titleLabel.text = item.title
        subtitleLabel.text = item.subtitle
    }

}
