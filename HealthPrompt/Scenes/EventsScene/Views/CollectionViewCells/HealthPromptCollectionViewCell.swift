//
//  HealthPromptCollectionViewCell.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit

class HealthPromptCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 10.0
        containerView.layer.masksToBounds = true
    }
    
    func configure(with item: HealthPromptItem) {
        messageLabel.text = item.message
    }
}
