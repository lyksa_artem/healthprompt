//
//  EventsViewController.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources

class EventsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var tableViewDataSource: RxTableViewSectionedReloadDataSource<EventSection>?
    private var collectionViewDataSource: RxCollectionViewSectionedReloadDataSource<HealthPromptSection>?
    
    var viewModel: EventsViewModel!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewsOnLoad()
        setupObservables()
    }
    
    private func setupViewsOnLoad() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44.0
        tableView.tableFooterView = UIView()
    }
    
    private func setupObservables() {
        tableViewDataSource = RxTableViewSectionedReloadDataSource<EventSection>(
            configureCell: { dataSource, tableView, indexPath, item in
                let cell = tableView.getCell(ofType: EventTableViewCell.self)
                cell.configure(with: item)
                return cell
        })
        
        collectionViewDataSource = RxCollectionViewSectionedReloadDataSource<HealthPromptSection>(
            configureCell: { dataSource, collectionView, indexPath, item in
                let cell = collectionView.getCell(ofType: HealthPromptCollectionViewCell.self, for: indexPath)
                cell.configure(with: item)
                return cell
        })
        
        tableView.rx
            .itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let event = self?.tableViewDataSource?[indexPath.section].items[indexPath.row] else {
                    return
                }
                self?.viewModel.select(event: event)
        }).disposed(by: disposeBag)
        
        tableView.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        collectionView.rx
            .setDelegate(self)
            .disposed(by: disposeBag)
        
        viewModel.eventSections
            .bind(to: tableView.rx.items(dataSource: tableViewDataSource!))
            .disposed(by: disposeBag)
        
        viewModel.healthPromptsSections
            .bind(to: collectionView.rx.items(dataSource: collectionViewDataSource!))
            .disposed(by: disposeBag)
        
        viewModel.getProfiles()
    }

}

extension EventsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.getHeaderFooterView(ofType: EventTableHeaderView.self)
        headerView.dateLabel.text = tableViewDataSource?.sectionModels[section].header
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
}

extension EventsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = collectionView.frame.width
        
        if let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout {
            width -= flowLayout.minimumLineSpacing
        }
        
        return CGSize(width: width, height: collectionView.frame.height)
    }
}
