//
//  EventSectionItem.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation
import RxDataSources

struct EventSection: SectionModelType {
    typealias Item = EventItem
    
    var header: String
    var items: [Item]
    
    init(original: EventSection, items: [Item]) {
        self = original
        self.items = items
        self.header = original.header
    }
    
    init(header: String, events: [TimelineEvent]) {
        self.header = header.components(separatedBy: "T").first ?? ""
        self.items = events.map({ EventItem(event: $0) })
    }
}

struct EventItem {
    let title: String
    let subtitle: String
    let uuid: String
    
    init(event: TimelineEvent) {
        self.title = event.category
        self.subtitle = event.description
        self.uuid = event.uuid
    }
}
