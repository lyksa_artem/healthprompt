//
//  HealthPromptSectionItem.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation
import RxDataSources

struct HealthPromptSection: SectionModelType {
    typealias Item = HealthPromptItem
    
    var items: [Item]
    
    init(original: HealthPromptSection, items: [Item]) {
        self = original
        self.items = items
    }
    
    init(healthPrompts: [HealthPrompt]) {
        self.items = healthPrompts.map({ HealthPromptItem(healthPrompt: $0) })
    }
}

struct HealthPromptItem {
    let message: String
    
    init(healthPrompt: HealthPrompt) {
        self.message = healthPrompt.message
    }
}
