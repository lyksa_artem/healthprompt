//
//  EventsViewModel.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class EventsViewModel: BaseViewModel {
    
    let networkService: NetworkService
    
    var eventSections: Observable<[EventSection]> {
        return eventsSectionsRelay.map({
            let sections = Dictionary(grouping: $0, by: { $0.timestamp })
            return sections.map({ EventSection(header: $0.key, events: $0.value) })
        })
    }
    
    var healthPromptsSections: Observable<[HealthPromptSection]> {
        return healthPromptsSectionsRelay.asObservable()
    }
    
    var selectedEvent: Observable<TimelineEvent> {
        return selectedEventSubject.asObservable()
    }
    
    private lazy var eventsSectionsRelay = BehaviorRelay<[TimelineEvent]>(value: [])
    private lazy var healthPromptsSectionsRelay = BehaviorRelay<[HealthPromptSection]>(value: [])
    
    private lazy var selectedEventSubject = PublishSubject<TimelineEvent>()
    
    init(networkService: NetworkService) {
        self.networkService = networkService
        super.init()
    }
    
    func getProfiles() {
        isLoadingSubject.onNext(true)
        
        networkService.getProfiles()
            .subscribe(onNext: { [weak self] profiles in
                self?.isLoadingSubject.onNext(false)
                guard let profile = profiles.first else {
                    self?.errorSubject.onNext(GenericError.profileNotFound)
                    return
                }
                self?.getProfileInfo(with: profile.profile_id)
            },onError: { [weak self] error in
                self?.errorSubject.onNext(error)
            }).disposed(by: disposeBag)
    }
    
    func select(event: EventItem) {
        if let timeline = eventsSectionsRelay.value.first(where: { $0.uuid == event.uuid }) {
            selectedEventSubject.onNext(timeline)
        }
    }
    
    private func getProfileInfo(with profile_id: String) {
        isLoadingSubject.onNext(true)
        
        Observable.zip(networkService.getProfileHealthPrompts(with: profile_id),
                       networkService.getProfileTimelineEvents(with: profile_id))
        .subscribe(onNext: { [weak self] healthPrompts, timeLineEvenets in
            self?.isLoadingSubject.onNext(false)
            self?.process(timelineEvents: timeLineEvenets)
            self?.process(healthPrompts: healthPrompts)
        }, onError: { [weak self] error in
            self?.errorSubject.onNext(error)
        }).disposed(by: disposeBag)
    }
    
    private func process(timelineEvents: [TimelineEvent]) {
        eventsSectionsRelay.accept(timelineEvents)
    }
    
    private func process(healthPrompts: [HealthPrompt]) {
        let dataSource = HealthPromptSection(healthPrompts: healthPrompts)
        healthPromptsSectionsRelay.accept([dataSource])
    }

}
