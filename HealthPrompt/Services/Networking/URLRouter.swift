//
//  URLRouter.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation
import Alamofire

enum URLRouter: URLRequestConvertible {
    
    private static let baseUrl = URL(string: "https://freemium.ottonova.de/api")!
    
    case getProfiles
    case getProfileTimelineEvents(profileId: String)
    case getProfileHealthPrompts(profileId: String)
    
    func asURLRequest() throws -> URLRequest {
        
        var httpMethod: HTTPMethod {
            switch self {
            case .getProfiles, .getProfileTimelineEvents, .getProfileHealthPrompts:
                return .get
            }
        }
        
        var url: URL {
            switch self {
            case .getProfiles:
                return URLRouter.baseUrl.appendingPathComponent("/user/customer/profiles")
            case .getProfileTimelineEvents(let profileId):
                return URLRouter.baseUrl.appendingPathComponent("/user/customer/profiles/\(profileId)/timeline-events")
            case .getProfileHealthPrompts(let profileId):
                return URLRouter.baseUrl.appendingPathComponent("/user/customer/profiles/\(profileId)/health-prompts")
            }
        }
        
        var params: Parameters? {
            return nil
        }
        
        let encoding = URLEncoding.queryString
        let request = try URLRequest(url: url, method: httpMethod)
        
        return try encoding.encode(request, with: params)
    }
    
}
