//
//  AlamofireService.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire

struct RequestRealizer: NetworkProtocol {
    
    func getProfiles() -> RequestResult {
        return performRequest(with: .getProfiles)
    }
    
    func getProfileTimelineEvents(with profileId: String) -> RequestResult {
        return performRequest(with: .getProfileTimelineEvents(profileId: profileId))
    }
    
    func getProfileHealthPrompts(with profileId: String) -> RequestResult {
        return performRequest(with: .getProfileHealthPrompts(profileId: profileId))
    }
    
    private func performRequest(with route: URLRouter) -> RequestResult {
        return RxAlamofire.requestData(route).map({ $0.1 })
    }
    
}
