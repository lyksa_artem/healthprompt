//
//  NetworkService.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

typealias RequestResult = Observable<Data>

protocol NetworkProtocol {
    
    func getProfiles() -> RequestResult
    func getProfileTimelineEvents(with profileId: String) -> RequestResult    
    func getProfileHealthPrompts(with profileId: String) -> RequestResult
}

struct NetworkService {
    
    private let requestRealizer: NetworkProtocol
    
    init(requestRealizer: NetworkProtocol) {
        self.requestRealizer = requestRealizer
    }
    
    func getProfiles() -> Observable<[Profile]> {
        return requestRealizer.getProfiles().toResult(type: [Profile].self)
    }
    
    func getProfileTimelineEvents(with profileId: String) -> Observable<[TimelineEvent]> {
        return requestRealizer.getProfileTimelineEvents(with: profileId).toResult(type: [TimelineEvent].self)
    }
    
    func getProfileHealthPrompts(with profileId: String) -> Observable<[HealthPrompt]> {
        return requestRealizer.getProfileHealthPrompts(with: profileId).toResult(type: [HealthPrompt].self)
    }
    
}

private extension Observable where Element == Data {
    
    func toResult<T: Decodable>(type: T.Type) -> Observable<T> {
        return map({
            try self.handleRequestResult(type: type, data: $0)
        })
    }
    
    private func handleRequestResult<T: Decodable>(type: T.Type, data: Data?) throws -> T {
        guard let data = data else {
            throw GenericError.cannotParseData
        }
        
        do {
            let response = try JSONDecoder().decode(type, from: data)
            return response
        } catch (let error ) {
            let nserror = error as NSError
            print("Error occurred when trying to parse \(type): \n ***\(nserror.userInfo)***")
            throw GenericError.generic(error)
        }
    }
}
