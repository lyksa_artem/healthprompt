//
//  TimelineEvent.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation

struct TimelineEvent: Decodable {
    let category: String
    let description: String
    let display_category: String
    let timestamp: String
    let title: String
    let uuid: String
}
