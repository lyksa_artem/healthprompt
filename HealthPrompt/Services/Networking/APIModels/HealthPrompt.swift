//
//  HealthPrompt.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation

struct HealthPrompt: Decodable {
    let display_category: String
    let message: String
    let metadata: Metadata
    let permanent: Bool
    let uuid: String
    let style: Style?
}

struct Metadata: Decodable {
    let link: Link
}

struct Link: Decodable {
    let title: String
    let url: String
}

struct Style: Decodable {
    let primary_color: String
    let secondary_color: String
    let image: String
}
