//
//  Profile.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import Foundation

struct Profile: Decodable {
    let profile_id: String
    let first_name: String
    let last_name: String
    let gender: String
    let display_name: String
    let is_primary_profile: Bool
    let tariff_label: String
    let date_of_birth: String
    let address: Address
    let contact: Contact
    let profile_attributes: [String]
}

struct Address: Decodable {
    let street: String
    let street_number: String
    let zip: String
    let city: String
}

struct Contact: Decodable {
    let email: String
    let phone: String
}
