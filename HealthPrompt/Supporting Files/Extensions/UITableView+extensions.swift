//
//  UITableView+extensions.swift
//  HealthPrompt
//
//  Created by Artem Lyksa on 3/27/19.
//  Copyright © 2019 lyksa. All rights reserved.
//

import UIKit

extension UITableView {
    
    func getCell<T>(ofType type: T.Type) -> T {
        
        let nibName = "\(type)"
        var cell = dequeueReusableCell(withIdentifier: nibName) as? T
        
        if cell == nil {
            register(UINib(nibName: nibName, bundle: nil), forCellReuseIdentifier: nibName)
            cell = dequeueReusableCell(withIdentifier: nibName) as? T
        }
        
        return cell!
    }
    
    func getHeaderFooterView<T>(ofType type: T.Type) -> T {
        
        let nibName = "\(type)"
        var view = dequeueReusableHeaderFooterView(withIdentifier: nibName) as? T
        
        if view == nil {
            register(UINib(nibName: nibName, bundle: nil), forHeaderFooterViewReuseIdentifier: nibName)
            view = dequeueReusableHeaderFooterView(withIdentifier: nibName) as? T
        }
        
        return view!
    }
}


